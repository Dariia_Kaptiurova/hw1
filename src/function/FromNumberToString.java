package com.company;

import java.util.Scanner;

public class FromNumberToString {

    public static final String[] LESS_THAN_TWENTY = {"ноль", "один", "два", "три", "четыре", "пять", "шесть", "семь", "восемь", "девять", "десять", "одинадцать", "двенадцадь", "тринадцать", "четырнадцать", "пятнадцать", "шестнадцать", "семнадцать", "восемнадцать", "девятнадцать"};
    public static final String[] DECADES = {"", "десять", "двадцать", "тридцать", "сорок", "пятьдесят", "шестьдесят", "семьдесят", "восемьдесят", "девяносто"};
    public static final String[] HUNDREDS = {"", "сто", "двести", "триста", "четыреста", "пятьсот", "шестьсот", "семьсот", "восемьсот", "девятьсот"};


    public static void main(String[] args) {
//    }
//        public FromNumberToString() {

            Scanner scanner = new Scanner(System.in);
            System.out.print("Введите число от 0 до 100: ");
            scanner.hasNextInt();
            int number = scanner.nextInt();
            scanner.close();

            if (number < 20)
                System.out.println(LESS_THAN_TWENTY[number]);
            else if (number < 100) {
                int high = number / 10;
                int low = number % 10;
                String text = DECADES[high];
                if (low != 0)
                    text = text + " " + LESS_THAN_TWENTY[low];
                System.out.println(text);
            } else if (number < 1000) {
                int higher = number / 100;
                int lower = number % 100;
                String text = HUNDREDS[higher];
                if (lower != 0) {
                    int high = lower / 10;
                    int low = lower % 10;
                    text = HUNDREDS[higher] + " " + DECADES[high];
                    if (low != 0)
                        text = text + " " + LESS_THAN_TWENTY[low];
                    System.out.println(text);

                }
            } else
                System.out.println(DECADES[0]);


        }
}
