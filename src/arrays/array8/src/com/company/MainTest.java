package com.company;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MainTest {

    @Test
    void resultFirst() {
        int size = 4;
        int [] arr = {1,2,3,4};
        assertEquals("Реверс масиву 3,4,1,2", "Реверс масиву 3,4,1,2");
    }

    @Test
    void resultZero() {
        int size = 0;
        int [] arr = {};
        assertEquals(" ", " ");
    }

    @Test
    void resultNegative() {
        int size = -9;
        int [] arr = {};
        assertEquals(" ", " ");
    }
}