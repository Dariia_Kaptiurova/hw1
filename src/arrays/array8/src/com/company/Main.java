package com.company;

import java.util.Arrays;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Введіть розмір масиву: ");
        int size = scanner.nextInt();
        int[] arr = new int[size];
        System.out.print("Введіть число: ");
        for (int i = 0; i < size; i++) {
            arr[i] = scanner.nextInt();
        } int mid = size%2;
        for (int i = 0; i<size/2; i++){
            int x = arr[i];
            arr[i] = arr[size/2+i+mid];
            arr[size/2+i+mid]= x;
        }
              System.out.println("Реверс масиву" + Arrays.toString(arr));
    }
}

