package com.company;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Введіть розмір масиву: ");
        int size = scanner.nextInt();
        int[] arr = new int[size];
        System.out.print("Введіть число: ");
        for (int i = 0; i < size; i++) {
            arr[i] = scanner.nextInt();
        }
        int max = arr[0];
        int maxIndex = 0;
        for (int i = 1; i < size; i++) {
            if (arr[i] > max) {
                max = arr[i];
                maxIndex = i;
            }
        }
        System.out.println("maxIndex= " + maxIndex);
    }
}
