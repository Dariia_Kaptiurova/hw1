package com.company;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MainTest {

    @Test
    void resultFirst() {
        int size = 3;
        int [] arr = {4,3,5};
        assertEquals("min= 3", "min= 3");
    }

    @Test
    void resultZero() {
        int size = 0;
        int [] arr = {};
        assertEquals(" ", " ");
    }

    @Test
    void resultNegative() {
        int size = -3;
        int [] arr = {};
        assertEquals(" ", " ");
    }
}