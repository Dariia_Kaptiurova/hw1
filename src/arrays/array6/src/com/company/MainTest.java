package com.company;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MainTest {

    @Test
    void resultFirst() {
        int size = 3;
        int [] arr = {4,8,5};
        assertEquals("Реверс масиву 5,8,4", "Реверс масиву 5,8,4");
    }

    @Test
    void resultZero() {
        int size = 0;
        int [] arr = {};
        assertEquals(" ", " ");
    }

    @Test
    void resultNegative() {
        int size = -9;
        int [] arr = {};
        assertEquals(" ", " ");
    }
}