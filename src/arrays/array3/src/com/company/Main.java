package com.company;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Введіть розмір масиву: ");
        int size = scanner.nextInt();
        int[] arr = new int[size];
        System.out.print("Введіть число: ");
        for (int i = 0; i < size; i++) {
            arr[i] = scanner.nextInt();
        }
        int minIndex = 0;
        int min = arr[0];
        for (int i = 1; i < size; i++) {
            if (arr[i] < min) {
                min = arr[i];
                minIndex = i;
            }
        }
        System.out.println("minIndex= " + minIndex);
    }
}
