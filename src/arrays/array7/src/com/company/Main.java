package com.company;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Введіть розмір масиву: ");
        int size = scanner.nextInt();
        int[] arr = new int[size];
        System.out.print("Введіть число: ");
        for (int i = 0; i < size; i++) {
            arr[i] = scanner.nextInt();
        }
        int kil = 0;
        for (int i = 0; i < size; i++) {
            if (i % 2 - 1 == 0)
                kil++;
        }
        System.out.println("Кількість непарних елементів:" + kil);
    }
}
