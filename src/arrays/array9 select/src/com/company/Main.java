package com.company;

import java.util.Arrays;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Введіть розмір масиву: ");
        int size = scanner.nextInt();
        int[] arr = new int[size];
        System.out.print("Введіть число: ");
        for (int i = 0; i < size; i++) {
            arr[i] = scanner.nextInt();
        }
        for (int left = 0; left < size; left++) {
            int min = left;
            for (int i = left+1; i < size; i++) {
                if (arr[i] < arr[min]) {
                    min = i;
                }
            }
            int a = arr[left];
            arr[left] = arr[min];
            arr[min] = a;
        }
        System.out.println(Arrays.toString(arr));
    }
}
