package com.company;

import java.util.Scanner;

import static java.lang.Math.sqrt;

public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("a = ");
        int a = scanner.nextInt();
        for (int i = 2; i <= sqrt(a); i++) {
            if (a % i == 0) {
                System.out.print("Число не являється простим");
                break;
            } else System.out.print("Число просте");
        }
    }
}
