package com.company;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MainTest {

    @Test
    void result1() {

        int a=6 ;
        assertEquals("Число не являється простим", "Число не являється простим");
    }

    @Test
    void resultSimple() {

        int a=1 ;
        assertEquals("Число просте", "Число просте");
    }

    @Test
    void resultSimpleZero() {

        int a=0 ;
        assertEquals("Число просте", "Число просте");
    }

}