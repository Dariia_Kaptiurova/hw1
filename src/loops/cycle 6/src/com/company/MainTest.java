package com.company;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MainTest {
    @Test
    void resultPositive() {

        int a=1234 ;
        assertEquals(4321, 4321);
    }

    @Test
    void resultZero() {

        int a=0 ;
        assertEquals(0, 0);
    }

    @Test
    void resultNegative() {

        int a=-8765 ;
        assertEquals(5678, 5678);
    }
}