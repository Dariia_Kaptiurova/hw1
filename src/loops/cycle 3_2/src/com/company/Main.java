package com.company;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("a = ");
        int a = scanner.nextInt();
        int first = 0;
        int last = a - 1;
        int mid = 0;
        while (last - first > 1) {
            mid = (last + first) / 2;

            if (mid * mid >= a) {
                last = mid;

            } else first = mid;
        }
            System.out.println(last);
    }
}

