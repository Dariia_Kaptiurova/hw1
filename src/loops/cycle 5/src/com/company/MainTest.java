package com.company;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MainTest {

    @Test
    void resultPositive() {

        int a=123 ;
        assertEquals(6, 6);
    }

    @Test
    void resultZero() {

        int a=0 ;
        assertEquals(0, 0);
    }

    @Test
    void resultNegative() {

        int a=-56 ;
        assertEquals(11, 11);
    }

}