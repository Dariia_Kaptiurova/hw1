package com.company;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("a = ");
        int a = scanner.nextInt();
        int sum = 0;
        for (; a > 0; a /= 10) {
            sum += a % 10;
        }
        System.out.println(sum + "");
    }
}
