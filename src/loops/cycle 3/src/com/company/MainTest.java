package com.company;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MainTest {

    @Test
    void resultPositive() {

        int a=4 ;
        assertEquals(2, 2);
    }

    @Test
    void resultZero() {

        int a=0 ;
        assertEquals("", "");
    }

    @Test
    void resultNegative() {

        int a=-9 ;
        assertEquals("", "");
    }
}