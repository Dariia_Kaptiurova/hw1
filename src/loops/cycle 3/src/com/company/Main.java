package com.company;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("a = ");
        int a = scanner.nextInt();
        for (int i = 1; i<a; i++){
            int b = i * i;
            if (a == b) {
                System.out.println(i);
                break;
            } else if (a < b) {
                System.out.println(i - 1);
                break;
            }
        }
    }
}

