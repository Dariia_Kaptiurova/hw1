package com.company;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("a = ");
        int a = scanner.nextInt();
        int b = 1;
        for (int i = 1; i <= a; i++) {
            b = b * i;
        }
        System.out.println(b);
    }
}
