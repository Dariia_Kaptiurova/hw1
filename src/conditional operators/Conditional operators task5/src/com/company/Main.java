package com.company;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        System.out.print("Рейтинг студента = ");
        int a = scanner.nextInt();
        if (a <= 19 & a >= 0) {
            System.out.print("F");
        } else if (a <= 39 & a >= 20) {
            System.out.print("E");
        } else if (a <= 59 & a >= 40) {
            System.out.print("D");
        } else if (a <= 74 & a >= 60) {
            System.out.print("C");
        } else if (a <= 89 & a >= 75) {
            System.out.print("B");
        } else if (a <= 100 & a >= 90) {
            System.out.print("A");
        } else System.out.println("Рейтинг має бути від 0 до 100");
    }
}
