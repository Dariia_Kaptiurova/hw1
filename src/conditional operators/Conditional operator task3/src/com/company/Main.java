package com.company;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("a= ");
        int a = scanner.nextInt();
        System.out.print("b= ");
        int b = scanner.nextInt();
        System.out.print("c= ");
        int c = scanner.nextInt();
        if (a > 0 && b > 0 && c > 0) {
            int sum = a + b + c;
            System.out.print(sum);
        } else if (a > 0 && b > 0 && c <= 0) {
            int sum = a + b;
            System.out.print(sum);
        } else if (a > 0 && b <= 0 && c > 0) {
            int sum = a + c;
            System.out.print(sum);
        } else if (a <= 0 && b > 0 && c > 0) {
            int sum = b + c;
            System.out.print(sum);
        } else if (a > 0 && b <= 0 && c <= 0) {
            int sum = a;
            System.out.print(sum);
        } else if (a <= 0 && b > 0 && c <= 0) {
            int sum = b;
            System.out.print(sum);
        } else if (a <= 0 && b <= 0 && c > 0) {
            int sum = c;
            System.out.print(sum);
        } else {
            System.out.print("0");
        }
    }
}
