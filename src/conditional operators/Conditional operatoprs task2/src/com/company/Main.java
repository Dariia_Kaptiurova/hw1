package com.company;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("x= ");
        int x = scanner.nextInt();
        System.out.print("y= ");
        int y = scanner.nextInt();
        if (x < 0 & y > 0) {
            System.out.print("Точка знаходиться в першій чверті");
        } else if (x > 0 & y > 0) {
            System.out.print("Точка знаходиться в другій чверті");
        } else if (x > 0 & y < 0) {
            System.out.print("Точка знаходиться в третій чверті");
        } else {
            System.out.println("Точка знаходиться в четвертій чверті");
        }
    }
}

