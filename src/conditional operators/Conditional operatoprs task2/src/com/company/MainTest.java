package com.company;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MainTest {
    @Test
    void resultFirstQuarter() {

        int x = -1;
        int y = 2;
        assertEquals("Точка знаходиться в першій чверті","Точка знаходиться в першій чверті");
    }

    @Test
    void resultSecondQuarter() {

        int x = 1;
        int y = 2;
        assertEquals("Точка знаходиться в другій чверті","Точка знаходиться в другій чверті");
    }

    @Test
    void resultThirdQuarter() {

        int x = 1;
        int y = -2;
        assertEquals("Точка знаходиться в третій чверті","Точка знаходиться в третій чверті");
    }

    @Test
    void resultFourthQuarter() {

        int x = -1;
        int y = -2;
        assertEquals("Точка знаходиться в четвертій чверті","Точка знаходиться в четвертій чверті");
    }


}