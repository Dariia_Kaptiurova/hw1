package com.company;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        System.out.print("a= ");
        int a = scanner.nextInt();
        System.out.print("b= ");
        int b = scanner.nextInt();
        if (a % 2 == 0) {
            int c = a * b;
            System.out.print("a*b=" + c);
        } else {
            int c = a + b;
            System.out.println("a+b=" + c);
        }
    }
}