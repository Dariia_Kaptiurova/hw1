package com.company;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MainTest {

    @Test
    void resultAdd() {

        int a = -1;
        int b = 2;
        assertEquals(1, 1);
    }

    @Test
    void resultMultiplication() {

        int a = 4;
        int b = 2;
        assertEquals(8, 8);
    }

    @Test
    void resultAddFailed() {

        int a = 0;
        int b = 0;
        assertEquals(0,0);
    }

}

